<?php 
include 'config/db.php';
    $name = $_GET['name'];
    $result = mysql_query("SELECT * FROM roles WHERE name='$name'");
    $row = mysql_fetch_assoc($result);
    if ( ! ($row)) {
        header('Location: index.php');
    exit();
    }
    $roleId = $row['id'];
 ?>
<!-- BEGIN PAGE TITLE & BREADCRUMB-->
                        <h3 class="page-title">
                            Permission <small>configure permission to <b><?php echo $row['name'] ?></b></small>
                        </h3>
                        <ul class="breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="index.php">Home</a> 
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a href="role_management.php">Role Management</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a href="new_role.php">New</a>
                                <i class="icon-angle-right"></i>
                            </li>
                            <li>
                                <a href="permission.php?name=<?php echo $name ?>">Permission</a>
                            </li>
                        </ul>
                        <!-- END PAGE TITLE & BREADCRUMB-->
                    </div>
                </div>
                <!-- END PAGE HEADER-->

                <!-- BEGIN PAGE CONTENT-->
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="portlet box light-grey">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-inbox"></i>Assign permission</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            
                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <div class="row-fluid">
                                  <div id="success" class="alert alert-block alert-success fade in" style="display:none;">  
                                    <strong> Changes have been saved ! </strong>
                                  </div>
                                </div>

                                    <div class="row-fluid">
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Add new module to <b><?php echo $name ?></b></label>
                                                <form action="new_permission.php" method="POST">
                                                <div class="controls">
                                                    <input type="hidden" name="roleId" id="id4role" value="<?php echo $row['id'] ?>">
                                                        <select name="module">
                                                        <?php $result = mysql_query("SELECT * FROM modules WHERE id NOT IN (SELECT moduleId FROM permissions WHERE roleId = '$roleId')") ?>
                                                            <option>Please select</option>
                                                        <?php while ($rowModule = mysql_fetch_assoc($result)): ?>
                                                            <option value="<?php echo $rowModule['id'] ?>"><?php echo $rowModule['name'] ?></option>
                                                        <?php endwhile; ?>
                                                        </select><br>
                                                    <button type="submit" class="btn green"><i class="icon-ok"></i>Assign</button><br>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="span6 ">
                                            <div class="control-group">
                                                <label class="control-label">Change <b><?php echo $name ?></b> permission</label>
                                                  
                                                 
                                                    <?php $result = mysql_query("SELECT p.id, p.moduleId, m.name, m.shortName FROM permissions AS p, modules AS m WHERE p.moduleId = m.id AND roleId = '$roleId'"); ?>
                                                    <?php while($rowPerm = mysql_fetch_assoc($result)): ?>
                                                        <div class="controls module">  
                                                        <input type="hidden" name="perm_id" class="perm_id" value="<?php echo $rowPerm['id']; ?>">        
                                                        <legend><?php echo $rowPerm['name']; ?>
                                                        <div class="span2"><h5 class="remove-module" src="delete_module.php?id=<?php echo $rowPerm['id'] ?> "><i class="icon icon-remove"></i> Remove</h5></div></legend>
                                                            <?php if ($rowPerm['shortName'] != 'calendar' AND $rowPerm['shortName'] != 'editableCalendar' AND $rowPerm['shortName'] != 'workingTime'):?>
                                                                <p><label class="checkbox" >
                                                                <input id="detail<?php echo $rowPerm['shortName']; ?>" <?php roleHasPermTo($rowPerm['id'],'detail',$rowPerm['shortName']); ?> type="checkbox" name="descriptions[]" value="detail_<?php  echo $rowPerm['shortName']; ?>" />
                                                                  Detail</label></p>

                                                                <p><label class="checkbox" >
                                                                <input id="edit<?php echo $rowPerm['shortName']; ?>" <?php roleHasPermTo($rowPerm['id'],'edit',$rowPerm['shortName']); ?>  type="checkbox" name="descriptions[]" value="edit_<?php  echo $rowPerm['shortName']; ?>"/>
                                                                  Edit</label></p>

                                                                <p><label class="checkbox" >
                                                                <input id="delete<?php echo $rowPerm['shortName']; ?>" <?php roleHasPermTo($rowPerm['id'],'delete',$rowPerm['shortName']); ?>  type="checkbox" name="descriptions[]" value="delete_<?php  echo $rowPerm['shortName']; ?>" />
                                                                  Delete</label></p> 
                                                            <?php endif; ?>

                                                        </div>        
                                                    <?php endwhile; ?>
                                                
                                                
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn green savePerm"><i class="icon-ok"></i> Save</button>
                                    </div>
                              
                                <!-- END FORM-->                
                            </div>

                        </div>
                    </div>
                </div>
                <!-- END PAGE CONTENT-->
                                   
<?php include 'footer.php';   // Template file ?>
<script type="text/javascript">
  $(function(){
     $('.savePerm').click(function(){
          var check_item =[];          
          $(':checkbox:checked').each(function(index,element){
               var id = $(this).parents('.module').find('.perm_id').val();
               var desc = $(this).val();               
               check_item[index] ={id: id , description: desc }; 
               //console.log(check_item[index]);
          });       
          console.log(check_item);
          $.post('new_sub_permission.php', {data : check_item , id4role : $('#id4role').val() }, function(data) {
            console.log(data);
          });
          
          $('#success').stop(true,true).fadeIn('fast');          
          return false;
        });


       /* $("#success").click(function(){
          $(this).stop(true,true).fadeOut("fast");
        });*/

        $('.remove-module').click(function(){
           if(confirm('You are about to delete')){
               $.ajax({
                    type : 'GET',                        
                    url : $(this).attr('src'),
                    success : function(success){}    
               });
               $(this).parents('.module').fadeOut('fast');
            }
        });
        
  });  
</script>
