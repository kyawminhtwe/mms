<?php include 'config/db.php'; //header ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>
</head>
<?php
 $id = $_GET['id'];

    $result = mysql_query("SELECT * FROM role WHERE id = $id");
       
    $row = mysql_fetch_assoc($result);?>
<body>
        <?php include 'inc/top.php'; //top ?>
       
        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="index.php">Home</a> 
                        <i class="icon-angle-right"></i>
                    </li>
                    <li>
                        <i class="icon-globe"></i>
                        <a href="index.php">Mail Magazines System</a> 
                        <i class="icon-angle-right"></i>
                    </li>
                    <li><a href="important_list.php">Important Mail</a>
                    <i class="icon-angle-right"></i>
                    </li>
                    <li><a href="#">Detail</a></li>
                </ul>
                <!-- END PAGE HEADER-->
				<div class="row-fluid">
                    <div class="span12">
                        <!-- BEGIN EXAMPLE TABLE PORTLET-->
                        <div class="portlet box light-grey">
                            <div class="portlet-title">
                                <div class="caption"><i class="icon-inbox"></i>Role Detail</div>
                                <div class="tools">
                                    <a href="javascript:;" class="collapse"></a>
                                </div>
                            </div>
                            <div class="box-content">
                    <table class="table table-bordered table-striped">
                        
                        <tr>
                            <td> Role Name</td>
                            <td>
                                <span class="timeline-title"><?php echo $row['name'] ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>Role Description</td>
                            <td>
                                <span class="timeline-title"><?php echo $row['description'] ?></span>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>CreatedDate</td>
                            <td>
                                <span class="timeline-title"><?php echo date("M j, Y",$row['createdDate']) ?></span>
                            </td>
                        </tr>
                        <tr>
                            <td>UpdatedDate</td>
                            <td>
                                <span class="timeline-title"><?php echo date("M j, Y",$row['updatedDate']) ?></span>
                            </td>
                        </tr>
                        
                    </table>
                </div><!-- box-content -->
            </div><!-- content -->
        </div> <!-- row-fluid -->
    </div> <!-- container-fluid-full -->
            <!-- END Page Content -->
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
