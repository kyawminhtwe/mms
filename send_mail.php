<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<?php include 'config/db.php'; // database?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php
		    $query = "SELECT * FROM emails where emails.isSent = 1";
		    $result = mysql_query($query);
		    if(! $result )
		    {
		      die('Could not get data: ' . mysql_error());
		    }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Send Mail</a></li>
					</ul>
					
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon share"></i><span class="break"></span>Sent Mail List</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
									  <tr>
									  		<th>No.</th>
											<th>Subject</th>
											<th>To</th>
											<th>Body</th>
											<th>Sent Date</th>
											<th>Actions</th>
									  </tr>
								 	</thead>
								 	<tbody>
									  	<?php 

								  	       	$i=1;
                                          	while ($row=mysql_fetch_assoc($result,MYSQL_ASSOC)) :
									  	?>
										<tr class="warning">
											<td><?php echo $i++?></td>
											<td class="center"><?php echo $row['subject']?></td>
											<td class="center"><?php echo $row['mail_to']?></td>
											<td class="center"><?php echo $row['body']?></td>
											<td class="center"><?php echo date("Y M, j", $row['send_date'])?></td>
											<td class="span2 text-center">
												<a class="btn btn-success" href="send_mail_detail.php?id=<?php echo $row['id']?>" title="Detail" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-danger" href="send_mail_delete.php?id=<?php echo $row['id']?>" title="Delete" data-rel="tooltip"  onclick="return confirm('Are you sure to delete this email..')">
													<i class="halflings-icon white trash"></i> 
												</a>
												</div>
											</td>
										</tr>
								 		<?php endwhile;?>
					  				</tbody>   
								</table>            
							</div><!-- box-content -->
						</div><!--/span-->
					</div><!--/row-->
			</div>
			<!-- end: Content -->
			
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
