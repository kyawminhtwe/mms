<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<?php include 'config/db.php'; // database?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php
		    $query = "SELECT * FROM emails where isStar=1";
		    $result = mysql_query($query);
		    if(! $result )
		    {
		      die('Could not get data: ' . mysql_error());
		    }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="important_list.php">Important</a></li>
					</ul>
					
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon star"></i><span class="break"></span>Important Mails</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
										<tr>
									  		<th>No.</th>
											<th>Status</th>
											<th>Subject</th>
											<th>From</th>
											<th>Text</th>
											<th>Actions</th>
										</tr>
								 	</thead>   
								  	<tbody>
								  		<?php
					                        $i = 1;
					                        while($row = mysql_fetch_assoc($result, MYSQL_ASSOC)):
				                        ?>
										
								 		<tr class="error">
											<td><?php echo $i++?></td>
											<td class="center">
												<span class="messagesList">
													<i class="glyphicons-icon star"></i>
												</span>
											</td>

											<td class="center"><?php echo $row['subject'] ?></td>
											<td class="center"><?php echo $row['mail_from'] ?></td>
											<td class="center"><?php echo $row['body'] ?></td>
											<td class="span2 text-center">
												<a class="btn btn-success" href="important_list_detail.php?id=<?php echo $row['id']?>" title="Detail" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-danger" href="important_list_delete.php?id=<?php echo $row['id']?>" title="Delete" data-rel="tooltip" onclick="return confirm('Are you sure to delete this email..')">
													<i class="halflings-icon white trash"></i> 
												</a>
												</div>
											</td>
										</tr>
								 		<?php   endwhile;   ?>
					  				</tbody>
					  			</table>            
							</div><!-- box-content -->
						</div><!--/span-->
					</div><!--/row-->
			</div><!-- end: Content -->
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
