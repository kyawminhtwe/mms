<?php

$template = array(
    'name'          => 'MMS',
    'version'       => '1.0',
    'author'        => 'MOON',
    'title'         => 'Mail Magazines System',
    'description'   => 'MOON means Myapanyi, zunenonOOo and kyawmiNhtwe',
    // 'fixed-top'         for a top fixed header
    // 'fixed-bottom'      for a bottom fixed header
    // ''                  empty for a static header
    'header'        => '',
    // 'sticky'            for a sticky sidebar
    'sidebar'       => 'sticky',
    // 'hide-side-content' for hiding sidebar by default
    'side_content'  => '',
    // 'full-width'        for full width page
    // ''                  empty to remove full width from the page in large resolutions
    'page'          => 'full-width',
    // Available themes: 'fire', 'wood', 'ocean', 'leaf', 'tulip', 'amethyst',
    //                   'dawn', 'city', 'oil', 'deepsea', 'stone', 'grass',
    //                   'army', 'autumn', 'night', 'diamond', 'cherry', 'sun'
    //                   'asphalt'
    
    'theme'         => '',

    'active_page'   => basename($_SERVER['PHP_SELF'])
);

// Primary navigation array (the primary navigation will be created automatically based on this array, up to 3 level deep)
?>
 
<div id="sidebar-left" class="span2">
    </br>
    </br>
        <div align="center">
            <form action="compose.php">
                <button type="submit" class="btn btn-danger">Compose</button>
            </form>
        </div>
    </br>
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="index.php"><i class="icon-envelope"></i><span class="hidden-tablet"> Inbox</span></a></li>
            <li><a href="user_list.php"><i class="icon-user"></i><span class="hidden-tablet"> User</span></a></li>
            <li><a href="template_list.php"><i class="icon-tasks"></i><span class="hidden-tablet"> Template List</span></a></li>
            <li><a href="scenario_list.php"><i class="icon-list-alt"></i><span class="hidden-tablet"> Scenario List</span></a></li>
            <li><a href="reply_mail.php"><i class="icon-comments-alt"></i><span class="hidden-tablet"> Reply Mail</span></a></li>
            <li><a href="send_mail.php"><i class="icon-share"></i><span class="hidden-tablet"> Sent Mail</span></a></li>
            <li><a href="important_list.php"><i class="icon-star"></i><span class="hidden-tablet"> Important</span></a></li>
            <li><a href="role_management.php"><i class="icon-user"></i><span class="hidden-tablet">Role Management</span></a></li>
         </ul>
    </div>
</div>
<!-- end: Main Menu -->