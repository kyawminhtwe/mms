<?php include 'config/db.php'; //header ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>

</head>

<body>
        <?php include 'inc/top.php'; //top ?>
       
        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="template_list.php">Template List</a>
                        <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Detail</a></li>
                    </ul>



<?php 
$id = $_GET['id'];
 if (filter_var($id, FILTER_VALIDATE_INT)) 
        {
         $id = $id;
        } else 
        {

         $id = false;
        }

if ($id == false) {
    $_SESSION['error2'] = "Please Input Category Information correctly.";
   header( 'location: index.php' ) ;
   exit();
   
} else {
$query = "SELECT * FROM templates WHERE id=$id";
$result = mysql_query($query);
$row = mysql_fetch_assoc($result);
}
 ?>

            
            <div class="box-content">
                        <table class="table table-bordered table-striped">
                            
                            <tr>
                                <td>Id</td>
                                <td>
                                    
                                    <span class="timeline-title"><?php echo $row['id'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Title</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['title'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['subject'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Header</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['header'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Body</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['body'] ?></span>
                                </td>
                            </tr>
                            
                                <td>CreatedDate</td>
                                <td>
                                    <span class="timeline-title"><?php echo date("M j, Y", $row['createdDate']) ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>UpdatedDate</td>
                                <td>
                                    <span class="timeline-title"><?php echo date("M j, Y", $row['createdDate']) ?></span>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                    </div>
                    </div> 
                    </div> 
            <!-- END Page Content -->
             <?php include 'inc/footer.php'; // Footer and scripts ?>
             <?php include 'inc/js.php'; //top ?>

