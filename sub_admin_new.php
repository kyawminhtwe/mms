<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			 <!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
			    </br>
			        <div align="center">
			            <form action="sub_admin_new.php">
			                <button type="submit" class="btn btn-danger">New Account</button>
			            </form>
			        </div>
			    </br>
			    <div class="nav-collapse sidebar-nav">
			        <ul class="nav nav-tabs nav-stacked main-menu">
			            <li><a href="subadmin_manage.php"><i class="icon-user"></i><span class="hidden-tablet"> Sub Admin List</span></a></li>
			            <li><a href="template_list.php"><i class="icon-lock"></i><span class="hidden-tablet"> Block   </span></a></li>
			            <li><a href="scenario_list.php"><i class="icon-cogs"></i><span class="hidden-tablet"> Functions</span></a></li>
			        </ul>
			    </div>
			</div>
			<!-- end: Main Menu -->
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-group"></i>
							<a href="sub_admin_manage.php">Sub-admin Manage</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="sub_admin_new.php">New</a></li>
					</ul>
					<!-- start: form -->

					<div class="row-fluid sortable">
					<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon edit"></i><span class="break"></span>Form Elements</h2>
						<div class="box-icon">
							<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal" action="do_new_subadmin.php" method="GET">
						  
						  	<div class="control-group">
							  <label class="control-label" for="typeahead">Name</label>
							  <div class="controls">
								<input type="text" name="name" id="username">
							  </div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Email</label>
							  <div class="controls">
								<input type="text" name="email" id="email" >
								</div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">ID</label>
							  <div class="controls">
								<input type="text" name="id" id="userid" >
								</div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Password</label>
							  <div class="controls">
								<input type="password" name="password" id="password">
								</div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Comfirm Password</label>
							  <div class="controls">
								<input type="password" name="cpassword" id="cpassword">
								</div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Phone</label>
							  <div class="controls">
								<input type="text" name="phone" id="phone">
								</div>
							</div>

							<div class="control-group">
							  <label class="control-label" for="typeahead">Address</label>
							  <div class="controls">
								 <textarea id="horizontal-textarea" name="address" rows="6"></textarea>
								</div>
							</div>
							
							<div class="control-group">
								<label class="control-label">Gender</label>
								<div class="controls">
								  <label class="radio">
									<input type="radio" name="gender" id="optionsRadios1" value="female" <?php if (isset($gender) && $gender=="female") echo "checked";?>>
									Female
								  </label>
								   
								  <label class="radio">
									<input type="radio" name="gender" id="optionsRadios2" value="male" <?php if (isset($gender) && $gender=="male") echo "checked";?>>
									Male
								  </label>
								</div>
							  </div>

							   <div class="control-group">
								<label class="control-label">Functions</label>
								<div class="controls">
								  <label>
									<input type="checkbox" id="inlineCheckbox1"  name="mail" value="1"> Mail Sending
								  </label>
								  <label >
									<input type="checkbox" id="inlineCheckbox2" name="template" value="1"  onclick="toggleSub(this, 'active_sub')"> Template Lists
								  </label>

								    <div id="active_sub">
								        <label class="checkbox inline"><input type="checkbox" name="templateEdit" value="1" /> Edit</label><br>
								        <label class="checkbox inline"><input type="checkbox" name="templateNew"  value="1" /> New</label><br>
								        <label class="checkbox inline"><input type="checkbox" name="templateDelete"  value="1" /> Delete</label>
								        
								    </div>
								    <label>
									<input type="checkbox" id="inlineCheckbox3" value="1"  name="scenario" onclick="toggleSub(this, 'active_sub2')"> Scenario Lists
								  </label>
								     <div id="active_sub2">
								        <label class="checkbox inline"><input type="checkbox" name="scenarioEdit" value="1" /> Edit</label><br>
								        <label class="checkbox inline"><input type="checkbox" name="scenarioNew" value="1" /> New</label><br>
								        <label class="checkbox inline"><input type="checkbox" name="scenarioDelete" value="1" /> Delete</label>
								        
								    </div>
								</div>
							  </div>

							<div class="form-actions">
							  <button type="submit" class="btn btn-primary">Create</button>&nbsp;
							  <button type="reset" class="btn">Reset</button>
							</div>
						 
						</form>   

					</div>
					</div><!--/span-->

					</div><!--/row-->
					<!-- end: form -->
					 
			</div>
			<!-- end: Content -->
			
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script type="text/javascript">
window.onload = function() {
   
    var checked = document.getElementById('inlineCheckbox2').checked;
    document.getElementById('active_sub').style.display = checked ? 'block' : 'none';
    var checked = document.getElementById('inlineCheckbox3').checked;
    document.getElementById('active_sub2').style.display = checked ? 'block' : 'none';

}

	function toggleSub(box, id) {
    // get reference to related content to display/hide
    var el = document.getElementById(id);
    
    if ( box.checked ) {
        el.style.display = 'block';
    } else {
        el.style.display = 'none';
    }
}

</script>
