<?php include 'config/db.php'; //header ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php 
              $query="SELECT * FROM subadmins";
              $result=mysql_query($query);
              if(! $result )
                        {
                            $_SESSION['error'] = "SQL Error ";
                            header("location: index.php");
                            exit();
                        }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			 <!-- start: Main Menu -->
			<div id="sidebar-left" class="span2">
			    </br>
			    </br>
			        <div align="center">
			            <form action="sub_admin_new.php">
			                <button type="submit" class="btn btn-danger">New Account</button>
			            </form>
			        </div>
			    </br>
			    <div class="nav-collapse sidebar-nav">
			        <ul class="nav nav-tabs nav-stacked main-menu">
			            <li><a href="sub_admin_manage.php"><i class="icon-group"></i><span class="hidden-tablet"> Sub Admin List</span></a></li>
			            <li><a href="template_list.php"><i class="icon-lock"></i><span class="hidden-tablet"> Block   </span></a></li>
			            <li><a href="scenario_list.php"><i class="icon-cogs"></i><span class="hidden-tablet"> Functions</span></a></li>
			        </ul>
			    </div>
			</div>
			<!-- end: Main Menu -->
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="sub_admin_manage.php">Sub-admin Manage</a></li>
					</ul>
					
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="icon-group"></i><span class="break"></span>Sub Admin List</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
									  <tr>
									  		<th>No.</th>
											<th>Name</th>
											<th>Email</th>
											<th>Phone</th>
											<th>Address</th>
											<th>Gender</th>
											<th>Actions</th>
									  </tr>
								 	</thead>   
								  	<tbody>
								  		<?php 

								  	       $i=1;
                                          while ($row=mysql_fetch_assoc($result,MYSQL_ASSOC)) :
								  		?>
										<tr>
											<td><?php echo $i++?></td>
											
											<td class="center"><?php echo $row['name']?></td>
											<td class="center"><?php echo $row['email']?></td>
											<td class="center"><?php echo $row['phone']?></td>
											<td class="center"><?php echo $row['address']?></td>
											<td class="center"><?php echo $row['gender']?></td>
											<td class="center">
												<a class="btn btn-success" href="#">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-info" href="#">
													<i class="halflings-icon white edit"></i>  
												</a>
												<a class="btn btn-danger" href="#">
													<i class="halflings-icon white trash"></i> 
												</a>
											</td>
										</tr>
								 		<?php endwhile;?>
					  				</tbody>
					  			</table>            
							</div>
						</div><!--/span-->
			
					</div><!--/row-->
			</div>
			<!-- end: Content -->
			
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
