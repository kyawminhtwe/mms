<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>
</head>

<body>
<?php include 'inc/top.php'; //top ?>
<?php
$id = $_GET['id'];
if (filter_var($id, FILTER_VALIDATE_INT)) 
    {
     $id = $id;
    }else { $id = false;}

if ($id == false) 
{

  $_SESSION['error2'] = "Information Incorrect";
  header( 'location: index.php' ) ;
  exit();
} 
else 
{
  $query = "SELECT * FROM scenarios where id=$id ";
  $result = mysql_query($query);
  $row = mysql_fetch_assoc($result);
}
?>

        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                         <li><a href="scenario_list.php">Scenario List</a>
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#"> Edit</a></li>
                    </ul>
                    <h2>Scenario Edit</h2>
                    
                    <div class="row-fluid sortable">        
                        <div class="box span12">
                          <div class="box-content">
                            <form action="do_edit_scenario.php" name="myForm" onsubmit="return validateForm()"  method="post" id="login-form" class="form-horizontal">
                              
                              <div class="control-group">
                                <label class="control-label" for="inline-text">Scenario Name</label>
                                <div class="controls">
                                    <input type="text" size ="30" id="userName" name="userName" value="<?php echo ($row['name'])?>"  >
                                </div>
                              </div>

                              <div class="control-group">
                                <label class="control-label" for="scenario">Genre</label>
                                <div class="controls">
                                  <select id="scenario" class="span6 typeahead" data-rel="chosen">
                                  <option value=0> Select Scenario </option>
                                  <option value=1> All </option>
                                  <option value=2> Job </option>
                                  <option value=3> Other </option>
                                  
                                  </select>
                                </div>
                              </div>
                              <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Edit</button>
                                <button type="reset"  class="btn">Cancel</button>
                              </div>
                            </form>
                          </div><!-- box-content -->
                        </div><!-- span -->
                    </div><!--/row-->
            </div>
            <!-- end: Content -->
            
        </div>
        </div>
        <!-- end: Header -->         

            
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script>
function validateForm() {
    var templateTitle = document.forms["myForm"]["templateTitle"].value;
    var subject = document.forms["myForm"]["subject"].value;    
     var body = document.forms["myForm"]["body"].value;
    
    if (templateTitle==null || templateTitle=="") {
       alert("Template Title must be filled out");
        return false;
    }
    if (subject==null || subject=="") {
        alert("subject must be filled out");
        return false;
    }
      if (body==null || body=="") {
        alert("Body must be filled out");
        return false;
    }
    
}
</script>