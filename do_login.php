<?php 
include("config/db.php");
session_start();
$userid = $_POST['userid'];
$password = $_POST['password'];
if ($userid && $password)
{

	// for prevention of sql injection 
	$query = sprintf("SELECT * FROM admins where user_id = '$userid' ",
		mysql_real_escape_string($userid));

		$result = mysql_query($query);
		$row = mysql_fetch_assoc($result);

		$salt = 'salt security code';
		$encryptedPassword = crypt($password,$salt);
		if ( ($row['password'] == $encryptedPassword) && ($row['user_id'] == $userid) ) {
			$_SESSION['auth'] = 'admin';
			$_SESSION['id'] = $row['id'];
			$_SESSION['userName'] = $row['name'];
			$_SESSION['lastActivity'] = time();

			if ($row['isFirstLogin'] == 0) {
				 
				header( 'Location: index.php' ) ;
				
			} else {
				header( 'Location: first_login.php' ) ;
			}

		}
		else{
			// $_SESSION["error"] = "Sorry, your email or password not correct.";
			header( 'Location: page_login1.php' ) ;
		}
}// end of if ($email && $password)
else{
	// $_SESSION["error"] = "You must fill email and password.";
	header( 'Location: page_login.php' ) ;
}


 ?>
 <!-- crypt('hello') => saPPmoXIbs91M -->