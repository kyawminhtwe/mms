<?php include 'config/db.php'; //header ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>

</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php 
              
              $query="SELECT * FROM emails";
              $result=mysql_query($query);
              if(! $result )
                        {
                            $_SESSION['error'] = "SQL Error ";
                            header("location: index.php");
                            exit();
                        }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Inbox</a></li>
					</ul>
					
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon envelope"></i><span class="break"></span>All Mails</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
									  <tr>
									  		<th>No.</th>
											<th>Status</th>
											<th>Subject</th>
											<th>From</th>
											<th>Text</th>
											<th>Actions</th>
									  </tr>
								 	 </thead>   
								  	<tbody>
								  	<?php 

								  	       $i=1;
                                          while ($row=mysql_fetch_assoc($result,MYSQL_ASSOC)) :
								  	?>
										<tr class=<?php if ($row['isReply'] == 1 ) echo "info"; ?>>
											<td><?php echo $i++?></td>
											<td class="center">
											<span class="messagesList">
												<i class="glyphicons-icon dislikes"></i>
												
											<ul></span>
											</td>
											
											<td class="center"><?php echo $row['subject']?></td>
											<td class="center"><?php echo $row['mail_from']?></td>
											<td class="center"><?php echo $row['body']?></td>
											<td class="span2 text-center">
												<div class="btn-group">
													<a class="btn btn-success" href="inbox_detail.php?id=<?php  echo $row['id'] ?>" title="Detail" data-rel="tooltip">
														<i class="halflings-icon white zoom-in"></i>  
													</a>
													<a class="btn btn-danger" href="inbox_delete.php?id=<?php  echo $row['id'] ?>"  title="Delete" data-rel="tooltip" onclick="return confirm('Are you sure to delete this email..')">
														<i class="halflings-icon white trash"></i> 
													</a>
												</div>
											</td>
										</tr>
									<?php endwhile;?>
					  				</tbody>
					  			</table>            
							</div>
						</div><!--/span-->
			
					</div><!--/row-->
			</div>
			<!-- end: Content -->
			
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script>
    $(function() {
        // Demostration of progress bars
        var pbButton = $('#example-progress-bar-button');
        var pbBar = $('#example-progress-bar .bar');

        // When the button is clicked
        pbButton.click(function() {
            $(this).button('loading');
            var i = 0;

            // Run the following block of code in intervals
            interval = setInterval(function() {
                pbBar.css('width', i + '%');
                if (i > 10)
                    pbBar.html(i + '%');
                if (i > 30)
                    pbBar.removeClass('bar-danger').addClass('bar-warning');
                if (i > 50)
                    pbBar.removeClass('bar-warning').addClass('bar-info');
                if (i > 70)
                    pbBar.removeClass('bar-info').addClass('bar-success');
                i += 5;
                if (i > 100) {
                    pbBar.css('width', '100%');
                    pbBar.html('<i class="icon-ok"></i>');
                    pbButton.html('Done!');
                    clearInterval(interval);
                }
            }, 300);
        });
    });
</script>
