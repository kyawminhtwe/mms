<?php include 'config/auth.php';   // authentication for logged in or not ?>
<?php include 'config/db.php'; // database connect to mysql 
	$id = $_SESSION['id'];
	$query = mysql_query("SELECT * FROM admins WHERE id='$id'");
	$rows = mysql_fetch_assoc($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<link href="css/bootstrap.min(2).css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="template_list.php">Template List</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">New</a>
						</li>
					</ul>
					<!-- start: form -->

					<div class="row-fluid sortable">
					<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Template Template</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
						<div class="box-content">
							<form class="form-horizontal" onsubmit="alert('Upload New Template Success');" action="do_new_template.php" method="post" enctype="multipart/form-data">
							  	<fieldset>
							  		
									<div class="control-group">
									  	<label class="control-label" >File Upload</label>
									  	<div class="controls">
											<input name="filename" type="file" required>
									  	</div>
									</div>    
									<div class="form-actions">
										<button type="reset" class="btn">Cancel</button>
									  	<button type="submit" id="btnSubmit" class="btn btn-primary" >Add</button>

									</div>
							  	</fieldset>
							</form>   
						</div>
					</div><!--/span-->
					</div><!--/row-->
					<!-- end: form -->
			</div>
			<!-- end: Content -->
		</div>
		</div>
		<!-- end: Header -->	
		<script>
   noty({ text: 'My first notification using noty'});
</script>	 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>

 
</body>
</html>
