<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php 
              
          $query="SELECT * FROM templates";
          $result=mysql_query($query);
          if(! $result )
            {
                $_SESSION['error'] = "SQL Error ";
                header("location: index.php");
                exit();
            }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Template List</a></li>
					</ul>
					<a class="btn btn-success" href="template_list_new.php"> 
						<b>Upload New Template</b> <!-- <input class="btn btn-success" id="fileInput" href="template_csv.php" type="file"> -->
					</a> 
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon tasks"></i><span class="break"></span>Template List</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
									  <tr>
									  		<th>No.</th>
											<th>Template Title</th>
											<th>Subject</th>
											<th>Body</th>
											<th>Actions</th>
									  </tr>
								 	</thead>   
								  	<tbody>
									  	<?php 

									  	    $i=1;
	                                        while ($row=mysql_fetch_assoc($result,MYSQL_ASSOC)) :
									  	?>
										<tr class="info">
											<td><?php echo $i++?></td>
											<td class="center"><?php echo $row['title']?></td>
											<td class="center"><?php echo $row['subject']?></td>
											<td class="center"><?php echo $row['body']?></td>
											<td class="span3 text-center">
												<a class="btn btn-success" href="template_list_detail.php?id=<?php echo $row['id']?>" title="Detail" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-info" href="template_list_edit.php?id=<?php echo $row['id']?>" title="Edit" data-rel="tooltip">
													<i class="halflings-icon white edit"></i>  
												</a>
												<a class="btn btn-danger" href="template_list_delete.php?id=<?php echo $row['id']?>" title="Delete" data-rel="tooltip"  onclick="return confirm('Are you sure to delete this template..')">
													<i class="halflings-icon white trash"></i> 
												</a>
												</div>
											</td>
										</tr>
								 		<?php endwhile;?>
					  				</tbody>
					  			</table>            
							</div><!-- box-content -->
						</div><!--/span-->
					</div><!--/row-->
			</div>
			<!-- end: Content -->
			
		</div>
		</div>
		<!-- end: Header -->	
		<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>CSV File Upload</h3>
		</div>
		<div class="modal-body">
			<form action="do_upload_csv.php" name="myForm" method="post" enctype="multipart/form-data" class="form-horizontal">
				<div class="control-group">
					<label class="control-label" for="csv_file">Upload CSV File</label>
					<div class="controls">
							<div id="uniform-fileInput" class="uploader">
								<input class="input-file uniform_on" name="csvFile" type="file">
								<span style="-moz-user-select: none;" class="filename">No file selected</span>
								<span style="-moz-user-select: none;" class="action">Choose File</span>
							</div>
					</div>
				</div>
				
			
				<div class="modal-footer">
					<!-- <a href="#" class="btn btn-primary">Upload</a>
					<a href="do_upload_csv.php" class="btn" data-dismiss="modal">Close</a> -->
		 			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	                <button type="submit" id="uploadButton" name="uploadButton" class="btn btn-primary">Upload</button>
				</div>
			</form>
		</div>
        
		

		
	</div>	 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
