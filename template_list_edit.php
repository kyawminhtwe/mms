<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>
</head>

<body>
      <?php include 'inc/top.php'; //top ?>
      <?php
          $id = $_GET['id'];
           if (filter_var($id, FILTER_VALIDATE_INT)) 
                {
                 $id = $id;
                } else 
                {
                  $id = false;
                }

          if ($id == false) {

              $_SESSION['error2'] = "Information Incorrect";
              header( 'location: index.php' ) ;
             exit();
          } else {
             $query = "SELECT * FROM templates where id=$id";
             $result = mysql_query($query);
             $row = mysql_fetch_assoc($result);
          }
      ?>

        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="template_list.php">Template list</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Edit</a></li>
                    </ul>
                   
                    <a class="btn btn-success" href="do_template_csv.php">
                        Upload Template 
                    </a>
                    <div></div>

                    <h1>Template Edit</h1>
                    <div class="row-fluid sortable">        
                        <div class="box span">
                          <div class="box-content">
                            <form action="do_edit_template.php" name="myForm" onsubmit="return validateForm()"  method="post" id="login-form" class="form-horizontal">
                                <div class="control-group">
                                  <label class="control-label" >Template Title</label>
                                  <div class="controls">
                                      <input type="text" id="templateTitle" name="templateTitle" value=<?php echo $row['title']?>  >
                             
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" >Subject</label>
                                  <div class="controls">
                                    <input type="text" id="subject" name="subject" value=<?php echo $row['subject']?> >
                                
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" >Header</label>
                                  <div class="controls">
                                     <input type="text" id="header" name="header" value=<?php echo $row['header']?>>
                       
                                  </div>
                                </div>
                                <div class="control-group hidden-phone">
                                  <label class="control-label" for="textarea" >Body</label>
                                  <div class="controls">
                                    <textarea id="textarea" name="body" rows="3" > <?php echo $row['body']?> </textarea>
                                  </div>
                                </div>
                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">Edit</button>
                                  <button type="reset"  class="btn">Cancel</button>
                                </div>
                            </form>
                          </div><!-- box-content -->
                        </div><!-- box span -->
                    </div><!--/row-->
            </div>
            <!-- end: Content -->
        </div>
        </div>
        <!-- end: Header -->         

            
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script>
function validateForm() {
    var templateTitle = document.forms["myForm"]["templateTitle"].value;
    var subject = document.forms["myForm"]["subject"].value;    
    var body = document.forms["myForm"]["body"].value;
    var header = document.forms["myForm"]["header"].value;
    
    if (templateTitle==null || templateTitle=="") {
       alert("Template Title must be filled out");
        return false;
    }
    if (subject==null || subject=="") {
        alert("subject must be filled out");
        return false;
    }
    if (body==null || body=="") {
        alert("Body must be filled out");
        return false;
    }
    if (header==null || header=="") {
        alert("Header must be filled out");
        return false;
    }
    
}
</script>
