<?php include 'config/db.php'; //header ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>

</head>

<body>
        <?php include 'inc/top.php'; //top ?>
       
        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="index.php">Inbox</a>
                        <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Detail</a></li>
                    </ul>
            <?php 
                    $id = $_GET['id'];
                     if (filter_var($id, FILTER_VALIDATE_INT)) 
                    {
                     $id = $id;
                    } else 
                    {

                     $id = false;
                    }

                    if ($id == false) {
                        $_SESSION['error2'] = "Please Input Category Information correctly.";
                       header( 'location: index.php' ) ;
                       exit();
                       
                    } else {
                            $query = "SELECT e.mail_from, e.mail_to, e.subject, e.body, e.send_date, e.sent_time, t.title as title, s.name as name,
                            e.createdDate 
                            FROM emails as e INNER JOIN templates as t INNER JOIN scenarios as s ON e.template_id=t.id and e.scenario_id=s.id and e.id=$id";
                            $result = mysql_query($query);
                            $row = mysql_fetch_assoc($result);
                    }
             ?>

            
            <div class="box-content">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <td>From</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['mail_from'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>To</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['mail_to'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Subject</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['subject'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Body</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['body'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Send_Date</td>
                                <td>
                                    <span class="timeline-title"><?php echo date("M j, Y",$row['send_date']) ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Send_Time</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['sent_time'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Template_id</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['title'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>Scenario_id</td>
                                <td>
                                    <span class="timeline-title"><?php echo $row['name'] ?></span>
                                </td>
                            </tr>
                            <tr>
                                <td>CreatedDate</td>
                                <td>
                                    <span class="timeline-title"><?php echo date("M j, Y", $row['createdDate']) ?></span>
                                </td>
                            </tr>
                           
                        </table>
                    </div>
                    </div>
                    </div> 
                    </div> 
            <!-- END Page Content -->
             <?php include 'inc/footer.php'; // Footer and scripts ?>
             <?php include 'inc/js.php'; //top ?>

