<?php
/**
 * footer.php
 *
 * Author: pixelcave
 *
 * The footer of the page
 * Jquery library as well as all other scripts are included here
 *
 */
?>
    <!-- Footer -->
    <!-- <footer>
        <div class="pull-right">
            Developed by <i class="icon-heart"></i> by <strong><a href="http://www.b4O.com" target="_blank">Kyaw Min Htwe,Thura Aung,Zune No No Oo</a></strong>
        </div>
        <div class="pull-left">
            <span id="year-copy"></span> &copy; <strong><a href="http://www.fist.com" target="_blank"><?php echo $template['name'] . ' ' . $template['version']; ?></a></strong>
        </div>
    </footer> -->
   
    <!-- END Footer -->
</div>
<!-- END Page Container -->

<!-- Scroll to top link, check main.js - scrollToTop() -->
<a href="#" id="to-top"><i class="icon-chevron-up"></i></a>

<!-- Getting admin information from database  -->
<?php 
//$id = $_SESSION['id'];
$query = mysql_query("SELECT * FROM admins WHERE id='1'");
$rows = mysql_fetch_assoc($query);
 ?>
<!-- End of Getting admin information from database  -->
<!-- User Modal Account, appears when clicking on 'User Settings' link found on user dropdown menu (header, top right) -->
<div id="modal-user-account" class="modal hide fade">
    <!-- Modal Body -->
    <div class="modal-body remove-padding">
        <!-- Modal Tabs -->
        <div class="block-tabs">
            <div class="block-options">
                <a href="javascript:void(0)" class="btn themed-color" data-dismiss="modal"><i class="icon-remove"></i></a>
            </div>
            <ul class="nav nav-tabs" data-toggle="tabs">
                <li class="active"><a href="#modal-user-account-account"><i class="icon-cog"></i> Account</a></li>
                <li><a href="#modal-user-account-profile"><i class="icon-user"></i> Profile</a></li>
            </ul>
            <div class="tab-content">
                <!-- Account Tab Content -->
                <div class="tab-pane active" id="modal-user-account-account">
                    <form action="do_update_account.php" method="post" class="form-horizontal" enctype="multipart/form-data" >
                        <div class="control-group">
                            <label class="control-label" for="modal-account-username">Username</label>
                            <div class="controls">
                                <input type="text" id="modal-account-username" value="admin" class="disabled" disabled>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="modal-account-pass">Current Password</label>
                            <div class="controls">
                                <input type="password" id="modal-account-pass" name="currentPass"  required>
                            </div>
                        </div>
                        <h4 class="sub-header">Change Email</h4>
                        <div class="control-group">
                            <label class="control-label" for="modal-account-email">Email</label>
                            <div class="controls">
                                <input type="text" id="u_email" name="email" value=<?php echo $rows['email']; ?> onkeyup="showHint(this.value)">
                            </div>
                        </div>
                        <h4 class="sub-header">Change Password</h4>
                        
                        <div class="control-group">
                            <label class="control-label" for="modal-account-newpass">New Password</label>
                            <div class="controls">
                                <input type="password" id="modal-account-newpass" name="newPass" pattern=".{6,20}" placeholder="Enter 6-20 characters">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="modal-account-newrepass">Retype New Password</label>
                            <div class="controls">
                                <input type="password" id="modal-account-newrepass" name="retypeNewPass" pattern=".{6,20}"  placeholder="Enter 6-20 characters">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="reset" class="btn themed-color"><i class="icon-repeat"></i> Reset</button>
                            <button type="submit" class="btn themed-color"><i class="icon-ok"></i> Save</button>
                        </div>
                    </form>
                </div>
                <!-- END Account Tab Content -->

                <!-- Profile Tab Content -->
                <div class="tab-pane" id="modal-user-account-profile">
                    <form action="do_update_profile.php? $pass=<?php echo $rows['password']?>" method="post" class="form-horizontal" enctype="multipart/form-data" >
                        <div class="control-group">
                            <label class="control-label" for="modal-profile-name">Name</label>
                            <div class="controls">
                                <input type="text" id="modal-profile-name" name="uname" value=<?php echo $rows['name']; ?>>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="modal-profile-gender">Gender</label>
                            <div class="controls">
                                <select id="modal-profile-gender" name="gender">
                                <?php if ($rows['gender'] == "female"){ ?>
                                    <option value="male">Male</option>
                                    <option value="female" selected>Female</option>
                                <?php } else { ?>
                                    <option value="male" selected>Male</option>
                                    <option value="female">Female</option>
                                <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="modal-profile-name">Phone</label>
                            <div class="controls">
                                <input type="text" id="modal-profile-name" name="phone" value=<?php echo $rows['phone']; ?>>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="modal-profile-bio">Address</label>
                            <div class="controls">
                                <textarea id="modal-profile-bio" name="address" class="textarea-elastic" rows="3"><?php echo $rows['address']; ?></textarea>
                            </div>
                        </div>
                         <div class="form-actions">
                            <button type="reset" class="btn themed-color"><i class="icon-repeat"></i> Reset</button>
                            <button type="submit" class="btn themed-color"><i class="icon-ok"></i> Save</button>
                        </div>
                    </form>
                </div>
                <!-- END Profile Tab Content -->
            </div>
        </div>
        <!-- END Modal Tabs -->
    </div>
    <!-- END Modal Body -->

             
    <!-- END Modal footer -->
</div>
 
<!-- END User Modal Settings -->
<!-- Get Jquery library from Google ... -->
<script src="js/vendor/jquery-1.9.1.min.js"></script>
<!-- ... but if something goes wrong get Jquery from local file -->
<script>!window.jQuery && document.write(unescape('%3Cscript src="js/vendor/jquery-1.9.1.min.js"%3E%3C/script%3E'));</script>

<!-- Bootstrap.js -->
<script src="js/vendor/bootstrap.min.js"></script>

<!--
Include Google Maps API for global use.
If you don't want to use  Google Maps API globally, just remove this line and the gmaps.js plugin from js/plugins.js (you can put it in a seperate file)
Then iclude them both in the pages you would like to use the google maps functionality
-->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>

<!-- Jquery plugins and custom javascript code -->
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
<script>
window.onload = function() {
    var es = document.getElementById('u_email').value;
    showHint(es);
}
function showHint(str)
{
    
    if (str.length == 0) {
        return;
    } 
    else 
    {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var obj = xmlhttp.responseText;
                var returnValue = jQuery.parseJSON(obj);
                if (returnValue.err == 1)
                {
                    return;
                } else {
                    alert(returnValue.msg);
                    
                }
            }

        }
        xmlhttp.open("GET", "do_user_email_change.php?q=" + str, true);
        xmlhttp.send();
    }
}

</script>
<?php include ('inc/footer.php');?>