<?php include 'config/auth.php';   // authentication for logged in or not ?>
<?php include 'config/db.php'; // database connect to mysql 
	$id = $_SESSION['id'];
	$query = mysql_query("SELECT * FROM admins WHERE id='$id'");
	$rows = mysql_fetch_assoc($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<link href="css/bootstrap.min(2).css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Compose</a>
						</li>
					</ul>
					<!-- start: form -->

					<div class="row-fluid sortable">
					<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon envelope"></i><span class="break"></span>Mails Template</h2>
								<div class="box-icon">
									<a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
						<div class="box-content">
							<form class="form-horizontal" action="compose_send_mail.php" method="get" enctype="multipart/form-data">
							  	<fieldset>
								  	<?php  	
								  		$query = "SELECT * FROM templates";
	    								$result = mysql_query($query);
		    						?>
								  	<div class="control-group">
										<label class="control-label">Template Name</label>
										<div class="controls">
										 	<select id="template_choose" name="template_choose" class="span6 typeahead" data-rel="chosen" onchange="showData(this.value)">
										 		<option value="0"> Select Template </option>
										 		<?php while($row = mysql_fetch_assoc($result, MYSQL_ASSOC)): 
										 		?>
												<option value=<?php echo $row['id'] ?> > <?php echo $row['title']?></option>
												<?php endwhile;  ?>
											</select>
										</div>
									</div>

								  	<?php  	
								  		$squery = "SELECT * FROM genres";
	    								$sresult = mysql_query($squery);
		    						?>
								  	<div class="control-group">
										<label class="control-label">Scenario </label>
										<div class="controls">
										 	<select id="scenario_choose" name="scenario_choose" class="span6 typeahead" data-rel="chosen" >
										 		<option value="0"> Select Genre </option>
										 		<?php while($srow = mysql_fetch_assoc($sresult, MYSQL_ASSOC)): 
										 		?>
												<option value=<?php echo $srow['id'] ?> > <?php echo $srow['name']?></option>
												<?php endwhile;  ?>
											</select>
										</div>
									</div>
					
									<div class="control-group">
										<label class="control-label" >From</label>
										<div class="controls">
											<input type="text" name="email_from" class="span6 typeahead" id="fromemail" value= "<?php echo $rows['email']?>" readonly="readonly" >
										</div>
									</div>

									<div class="control-group">
									  	<label class="control-label" >Template Title</label>
									  	<div class="controls">
											<input type="text" name="template_title" class="span6 typeahead" id="templatetitle" value="<?php echo $row['title']?>" readonly="readonly" >
										</div>
									</div>

									<div class="control-group">
										<label class="control-label" >Header</label>
										<div class="controls">
											<input type="text" name="email_header" class="span6 typeahead" id="header" value="<?php echo $row['header']?>" readonly="readonly" >
											
										</div>
									</div>

									<div class="control-group">
									  	<label class="control-label" >Subject</label>
									  	<div class="controls">
											<input type="text" name="email_subject" class="span6 typeahead" id="subject"  value="<?php echo $row['subject']?>" readonly="readonly" >
										
									  	</div>
									</div>
									         
									<div class="control-group hidden-phone">
									  <label class="control-label" >Body</label>
									  <div class="controls">
										<textarea class="cleditor" name="email_body" id="textarea"  rows="3"></textarea>
									  </div>
									</div>

									<div class="control-group">
										<label class="control-label">Send Mail Date</label>
										<div class="controls">
											<label class="radio">
												<input type="radio" name="radio" id="now" value="0" >
												Now
											<label class="radio">
												<input type="radio" name="radio" id="time" value="1" checked="" >
												Time
										</div>
								  	</div>	

								  	<div class="control-group" id="chooseDate">
						                <label class="control-label">Send Date</label>
						                <div class="controls input-append date form_date" data-date="30-03-2015" data-link-field="dtp_input2" data-date-format="dd MM yyyy"  data-link-format="yyyy-mm-dd">
						                    <input  type="text" name="datevalue" value="" readonly="readonly" >
						                  	<span class="add-on"><i class="icon-th"></i></span>
										</div>
									</div>	 

									<div class="control-group" id="chooseTime">
						                <label class="control-label">Send Time </label>
						                <div class="controls input-append date form_time" data-time="30-03-2015" data-date-format="hh:ii"  data-link-format="hh:ii">
						                    <input type="text" name="timevalue" value="" readonly="readonly" >
						                  	<span class="add-on"><i class="icon-th"></i></span>
						                </div>
										 
						            </div>
								  	<div class="control-group">
										<label class="control-label" for="testmail">Test Mail</label>
										<div class="controls">
										  <div class="input-append">
											<input id="testmail" name="testemail" size="16" type="text" ><button class="btn" type="button">Test!</button>
										  </div>
										</div>
									</div>
									<!-- <div class="control-group">
									  <label class="control-label" >Test Mail</label>
									  <div class="controls">
										<input type="text" name="testmail" class="span6 typeahead" id="testmail" >
										<button type="submit" class="btn btn-primary">Test</button>
									  </div>
									</div> -->
									<div class="form-actions">
										<button type="reset" class="btn">Cancel</button>
									  	<button type="submit" class="btn btn-primary">Send</button>
									</div>
							  	</fieldset>
							</form>   
						</div>
					</div><!--/span-->
					</div><!--/row-->
					<!-- end: form -->
			</div>
			<!-- end: Content -->
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
<script>
 
window.onload = function() {
	 $("input[type=radio]").change(function() {  
        if($(this).prop('value') == 'now'){
                $("#chooseTime").hide();
                $("#chooseDate").hide();
               
            }
            else {
                $("#chooseTime").show();
                $("#chooseDate").show();
            }
    });
    // var user = document.getElementById('inputUsername').value;
    // showHint(user);
}

function showData(str) {
	
    if (str == "0") {
    	var tt = document.getElementById("templatetitle");
        tt.value ="";
        var hd = document.getElementById("header");
        hd.value = "";
        var sub = document.getElementById("subject");
        sub.value = "";
        document.getElementById("btnSubmit").disabled = true;
       
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var obj = xmlhttp.responseText;
                var returnValue = jQuery.parseJSON(obj);
                if (returnValue.err == 1) {
                    document.getElementById("btnSubmit").disabled = true;
                } else {
                    var tt = document.getElementById("templatetitle");
                    tt.value = returnValue.templatetitle;
                    var hd = document.getElementById("header");
                    hd.value = returnValue.header;
                    var sub = document.getElementById("subject");
                    sub.value = returnValue.subject;
                    document.getElementById("scenario_choose").disabled = true;
                    document.getElementById("btnSubmit").disabled = false;
                    document.getElementById("template").disabled = true;
                }
            }
        }
        xmlhttp.open("GET", "compose_showData.php?s=" + str, true);
        xmlhttp.send();
    }
}

function donow(str){
  if (str != "Select Scenario") {
        document.getElementById("template").disabled = true;
    } 
}

</script>
<!-- <script type="text/javascript" src="js/jquery-1.8.3.min.js" charset="UTF-8"></script> -->
<script type="text/javascript" src="js/bootstrap.min(2).js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        //language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		forceParse: 0,
        showMeridian: 1
    });
	$('.form_date').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('.form_time').datetimepicker({
        language:  'fr',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0
    });
</script>
</body>
</html>