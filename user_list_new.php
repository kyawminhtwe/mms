<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>
</head>
 <?php 
include 'config/db.php'; 

 ?>

<body>
      <?php include 'inc/top.php'; //top ?>
     
        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="user_list.php">User list</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Edit</a></li>
                    </ul>
                   
                    
                    <div></div>

                    <h1>Create User</h1>
                    <div class="row-fluid sortable">        
                        <div class="box span">
                          <div class="box-content">
                            <form action="do_new_user.php" name="myForm" onsubmit="return validateForm()"  method="get" id="login-form" class="form-horizontal">
                                <div class="control-group">
                                  <label class="control-label" >Name</label>
                                  <div class="controls">
                                      <input type="text"  name="name"   >
                             
                                  </div>
                                </div>
                                <div class="control-group">
                                    <label class="control-label" for="val_email">Email *</label>
                                    <div class="controls">
                                        <div class="input-prepend">
                                            <span class="add-on"><i class="icon-envelope"></i></span>
                                            <input type="text" onkeyup="checkEmail(this.value)" id="val_email" name="email" required>
                                        <!--     <span class="error" id="txtEmail"></span> -->
                                        </div>
                                    </div>
                                </div>                                            
                                                             
                                <div class="control-group">
                                    <label class="control-label" for="example-advanced-firstname">Password</label>
                                    <div class="controls">
                                        <input type="text" id="example-advanced-firstname" name="password">
                                    </div>
                                </div>
                                <div class="control-group">
									<label class="control-label" >Role Name</label>
									<div class="controls">
										<?php 
											$query = "SELECT id, name FROM role";
		   									 $result = mysql_query($query);
										?>
									  <select name="roleId">
										  <option value="0">Please Select</option>>
											<?php while ($row = mysql_fetch_assoc($result)) :?>
		                                                <option value="<?php echo $row['id'] ?>"><?php echo $row['name'] ?>
		                                                </option>
		                                    <?php endwhile; ?>
	                                    </select>
									</div>
							  	</div>

                                <div class="control-group">
                                  <label class="control-label">Gender</label>
                                  <div class="controls">
                                    <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios1" value="female" checked>
                                    Female
                                    </label>
                                     
                                    <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios2" value="male">
                                    Male
                                    </label>
                                  </div>
                                  </div>

                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">Edit</button>
                                  <button type="reset"  class="btn">Cancel</button>
                                </div>
                            </form>
                          </div><!-- box-content -->
                        </div><!-- box span -->
                    </div><!--/row-->
            </div>
            <!-- end: Content -->
        </div>
        </div>
        <!-- end: Header -->         

            
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script>
function validateForm() {
    var name = document.forms["myForm"]["name"].value;
    var email = document.forms["myForm"]["email"].value;    
   
    var password = document.forms["myForm"]["password"].value;
    var roleId = document.forms["myForm"]["roleId"].value;
    
    
    
    if (name==null || name=="") {
       alert("Name must be filled out");
        return false;
    }
    if (email==null || email=="") {
        alert("Email must be filled out");
        return false;
    }
    
    if (password==null || password=="") {
        alert("password must be filled out");
        return false;
    }
    if (roleId==null || roleId=="") {
        alert("roleNumb must be filled out");
        return false;
    }
    
    
}
</script>
