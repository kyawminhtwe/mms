<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php';?>
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
			<style type="text/css">
			body { background: url(img/bg-login.jpg) !important; }
		</style>
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
					
			<div class="row-fluid">
				<div class="login-box">
					<div class="icons">
						<a href="login.php"><i class="halflings-icon refresh"></i></a>
				
					</div>
					<h2>Login to your account</h2>
					<form class="form-horizontal" action="do_login.php" method="post">
						<fieldset>
							
							<div class="input-prepend" title="Username">
								<span class="add-on"><i class="halflings-icon user"></i></span>
								<input class="input-large span10" name="userid" id="userid" type="text" placeholder="type user id"/>
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password">
								<span class="add-on"><i class="halflings-icon lock"></i></span>
								<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
							</div>
							<div class="clearfix"></div>
							<div class="button-login">	
								<button type="submit" class="btn btn-primary">Login</button>
							</div>
							<div class="clearfix"></div>
					</form>
					<hr>
					<h3>Forgot Password?</h3>
					<p>
						No problem, <a href="#" class="btn-setting"><span style="color: red;"><u>click here</u></span></a> to get a new password.
					</p>	
				</div><!--/span-->
			</div><!--/row-->
			

	</div><!--/.fluid-container-->
	</div><!--/fluid-row-->

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3>Password Reminder</h3>
		</div>
		<form action="forget_password.php" method="post">
			<div class="modal-body">
				<div class="control-group">
					<label class="control-label">Please, Enter Your email :      <input type="text" name="email" id="email"></label>
				</div>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Cancel</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<button type="submit" class="btn btn-primary">Get New Password</button>
			</div>
		</form>
	</div>
	<!-- start: JavaScript-->
	<?php include 'inc/js.php';?>
	
</body>
</html>
