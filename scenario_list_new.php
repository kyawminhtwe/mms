<?php include 'config/auth.php';   // authentication for logged in or not ?>
<?php include 'config/db.php'; // database connect to mysql 
	$id = $_SESSION['id'];
	$query = mysql_query("SELECT * FROM admins WHERE id='$id'");
	$rows = mysql_fetch_assoc($query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<link href="css/bootstrap.min(2).css" rel="stylesheet" media="screen">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
</head>

<body>
		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="scenario_list.php">Scenario List</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">New</a>
						</li>
					</ul>
					<!-- start: form -->

					<div class="row-fluid sortable">
					<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon list-alt"></i><span class="break"></span>Scenario Template</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
						<div class="box-content">
							<form class="form-horizontal" onsubmit="alert('Success');" action="do_new_scenario.php" method="post" enctype="multipart/form-data">
							  	<fieldset>
								  	<div class="control-group">
										<label class="control-label">Scenario Name</label>
										<div class="controls">
											<input type="text" class="span6 typeahead" name="s_name" id="sname" required>
										</div>
									</div>
								  	
								  	<div class="control-group">
								  		<label class="control-label" for="scenario">Genre</label>
										<div class="controls">
										  <select name="s_genre" id="scenario" class="span6 typeahead" data-rel="chosen" >
											
											<option value=1 selected="selected"> All </option>
											<option value=2> Job </option>
											<option value=3> Other </option>
											
										  </select>
										</div>
									</div>
									<div class="control-group">
									  	<label class="control-label" >File Upload</label>
									  	<div class="controls">
											<input name="filename" type="file" required>
									  	</div>
									</div>    
									<div class="form-actions">
										<button type="reset" class="btn">Cancel</button>
									  	<button type="submit" id="btnSubmit" class="btn btn-primary">Add</button>
									</div>
							  	</fieldset>
							</form>   
						</div>
					</div><!--/span-->
					</div><!--/row-->
					<!-- end: form -->
			</div>
			<!-- end: Content -->
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>

 
</body>
</html>