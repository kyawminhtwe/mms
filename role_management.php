<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
	<?php include 'config/db.php'; // database?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php
		    $query = "SELECT * FROM role";
		    $result = mysql_query($query);
		    if(! $result )
		    {
		      die('Could not get data: ' . mysql_error());
		    }
		?>
		<div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <a href="role_management.php">Role Management</a>
                    </ul>
                    <div class="portlet-body">
                                <div class="table-toolbar">
                                    <div class="btn-group">
                                        <a href="new_role.php" class="btn green">
                                        Add New <i class="icon-plus"></i>
                                        </a>
                                    </div>
                                </div>
				<div class="row-fluid sortable">        
                        <div class="box span12">
                            <div class="box-header" data-original-title>
                                <h2><i class="halflings-icon share"></i><span class="break"></span>Role List</h2>
                                <div class="box-icon">
                                    <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                                    <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
                                </div>
                            </div>
                            
                                <table class="table table-striped table-bordered table-hover" id="sample_1">
                                    <thead>
                                        <tr>
                                            <th style="width:8px;">#</th>
                                            <th>Role Name</th>
                                            <th>Description</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php $i = 0; ?>
                                    <?php while ($row = mysql_fetch_assoc($result)) :?>
                                        <tr class="odd gradeX">
                                            <td><?php echo ++$i; ?></td>
                                            <td><?php echo $row['name'] ?></td>
                                            <td><?php echo $row['description'] ?></td>
                                            <td >
                                                <a href="role_management_detail.php?id=<?php echo $row['id'] ?>" title="Detail" class="btn mini"><i class="icon-zoom-in"></i></a>
                                                <a href="role_management_edit.php?id=<?php echo $row['id'] ?>" title="Edit" class="btn mini blue"><i class="icon-edit"></i></a>
                                                <a href="permission.php?name=<?php echo $row['name'] ?>" title="Permission" class="btn mini green"><i class="icon-edit"></i></a>
                                                <a href="role_management_delete.php?id=<?php echo $row['id'] ?>"title="Delete" onclick="return confirm('Are you sure to delete?');" class="btn mini red"><i class="icon-trash"></i></a>
                                                
                                                                                       </td>
                                        </tr>
                                        <?php endwhile;?>
                                    </tbody>   
                                </table>            
                            </div><!-- box-content -->
                        </div><!--/span-->
                    </div><!--/row-->
            </div>
            <!-- end: Content -->
            
        </div>
        </div>
        <!-- end: Header -->         

            
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
