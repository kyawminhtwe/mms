<!DOCTYPE html>
<html lang="en">
<head>
	<?php include 'inc/header.php'; //header ?>
</head>

<body>
		<?php include 'inc/top.php'; //top ?>
		<?php 
              
              $query="SELECT * FROM scenarios";
              $result=mysql_query($query);
              if(! $result )
	            {
	                $_SESSION['error'] = "SQL Error ";
	                header("location: index.php");
	                exit();
	            }
		?>

		<div class="container-fluid-full">
		<div class="row-fluid">
			<?php include 'inc/config.php'; //side ?>
			<!-- start: content -->
			<div id="content" class="span10">
					<ul class="breadcrumb">
						<li>
							<i class="icon-home"></i>
							<a href="index.php">Home</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<i class="icon-globe"></i>
							<a href="index.php">Mail Magazines System</a> 
							<i class="icon-angle-right"></i>
						</li>
						<li><a href="#">Scenario List</a></li>
					</ul>
					
					<a class="btn btn-success" href="scenario_list_new.php">
						 New Scenario
					</a>
					<div class="row-fluid sortable">		
						<div class="box span12">
							<div class="box-header" data-original-title>
								<h2><i class="halflings-icon tasks"></i><span class="break"></span>Template List</h2>
								<div class="box-icon">
									<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
									<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
								</div>
							</div>
							<div class="box-content">
								<table class="table table-striped table-bordered bootstrap-datatable datatable">
								  	<thead>
										<tr>
									  		<th>No.</th>
											<th>Scenario Title</th>
											<th>Genre</th>
											<th>User Name</th>
											<th>User Email</th>
											<th>Actions</th>
										</tr>
								 	</thead>   
								  	<tbody>
									  	<?php 

								  	       	$j = 1;
                                          	while ($row=mysql_fetch_assoc($result,MYSQL_ASSOC)) :
									  	?>
									  	<?php if($row['is_star']==1) {?>
										<tr class="error">
											<td><?php echo $j++?></td>
											<td class="center"><?php echo $row['name']?></td>
											<td class="center"><?php echo $row['genre']?></td>
											<td class="center"><?php echo $row['user_name']?></td>
											<td class="center"><?php echo $row['user_email']?></td>
											<td class="span3 text-center">
												<a class="btn btn-success" href="scenario_list_detail.php?id=<?php echo $row['id']?>" title="Detail" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-info" href="scenario_list_edit.php?id=<?php echo $row['id']?>" title="Edit" data-rel="tooltip">
													<i class="halflings-icon white edit"></i>  
												</a>
												<a class="btn btn-danger" href="scenario_list_unimportant.php?id=<?php echo $row['id']?>" title="Unimportant" data-rel="tooltip" onclick="return confirm('Is Unimportant really??')"> 
													<i class=" halflings-icon white star" ></i>
												</a>
											</td>
										</tr>
										<?php } else { ?>
										<tr class="info">
											<td><?php echo $j++?></td>
											<td class="center"><?php echo $row['name']?></td>
											<td class="center"><?php echo $row['genre']?></td>
											<td class="center"><?php echo $row['user_name']?></td>
											<td class="center"><?php echo $row['user_email']?></td>
											<td class="span3 text-center">
												<a class="btn btn-success" href="scenario_list_detail.php?id=<?php echo $row['id']?>" title="Detail" data-rel="tooltip">
													<i class="halflings-icon white zoom-in"></i>  
												</a>
												<a class="btn btn-info" href="scenario_list_edit.php?id=<?php echo $row['id']?>" title="Edit" data-rel="tooltip">
													<i class="halflings-icon white edit"></i>  
												</a>
												<a class="btn btn-danger" href="scenario_list_delete.php?id=<?php echo $row['id']?>" title="Delete" data-rel="tooltip" onclick="return confirm('Are you sure to delete this Scenario..')">
													<i class="halflings-icon white trash"></i> 
												</a>
												<a class="btn btn-info" href="scenario_list_important.php?id=<?php echo $row['id']?>" title="Important" data-rel="tooltip" onclick="return confirm('Is important really??')"> 
													<i class=" halflings-icon white star" ></i>
												</a>
											</td>
										<?php } ?>
										</tr>
										<?php endwhile;?>
								 	</tbody>
				  					
					  			</table>            
							</div>
						</div><!--/span-->
			
					</div><!--/row-->
			</div>
			<!-- end: Content -->
		</div>
		</div>
		<!-- end: Header -->		 

			
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>

</body>
</html>
