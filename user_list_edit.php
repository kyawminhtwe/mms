<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'inc/header.php'; //header ?>
</head>

<body>
      <?php include 'inc/top.php'; //top ?>
      <?php
          $id = $_GET['id'];
           if (filter_var($id, FILTER_VALIDATE_INT)) 
                {
                 $id = $id;
                } else 
                {
                  $id = false;
                }

          if ($id == false) {

              $_SESSION['error2'] = "Information Incorrect";
              header( 'location: index.php' ) ;
             exit();
          } else {
             $result = mysql_query("SELECT id, name FROM roles");
            $id = $_GET['id'];
           $userResult = mysql_query("SELECT u.id, u.name, u.email,u.gender, r.name AS roleName FROM users AS u, roles AS r WHERE u.roleId = r.id and u.id = '$id' ");
           $row = mysql_fetch_assoc($userResult);
          }
      ?>

        <div class="container-fluid-full">
        <div class="row-fluid">
            <?php include 'inc/config.php'; //side ?>
            <!-- start: content -->
            <div id="content" class="span10">
                    <ul class="breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="index.php">Home</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <i class="icon-globe"></i>
                            <a href="index.php">Mail Magazines System</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="user_list.php">User list</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li><a href="#">Edit</a></li>
                    </ul>
                   
                    
                    <div></div>

                    <h1>Template Edit</h1>
                    <div class="row-fluid sortable">        
                        <div class="box span">
                          <div class="box-content">
                            <form action="do_edit_template.php" name="myForm" onsubmit="return validateForm()"  method="post" id="login-form" class="form-horizontal">
                                <div class="control-group">
                                  <label class="control-label" >Name</label>
                                  <div class="controls">
                                      <input type="text" id="templateTitle" name="name" value=<?php echo $row['name']?>  >
                             
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label" >Email</label>
                                  <div class="controls">
                                    <input type="text" id="subject" name="email" value=<?php echo $row['email']?> >
                                
                                  </div>
                                </div>
                                <div class="control-group">
                                  <label class="control-label">Gender</label>
                                  <div class="controls">
                                    <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios1" value="female" <?php if ($row['gender'] == "female") echo "checked"; ?>>
                                    Female
                                    </label>
                                     
                                    <label class="radio">
                                    <input type="radio" name="gender" id="optionsRadios2" value="male" <?php if ($row['gender'] == "male") echo "checked"; ?>>
                                    Male
                                    </label>
                                  </div>
                                  </div>

                                <div class="form-actions">
                                  <button type="submit" class="btn btn-primary">Edit</button>
                                  <button type="reset"  class="btn">Cancel</button>
                                </div>
                            </form>
                          </div><!-- box-content -->
                        </div><!-- box span -->
                    </div><!--/row-->
            </div>
            <!-- end: Content -->
        </div>
        </div>
        <!-- end: Header -->         

            
<?php include 'inc/footer.php'; // Footer and scripts ?>
<?php include 'inc/js.php'; //top ?>
</body>
</html>
<script>
function validateForm() {
    var name = document.forms["myForm"]["name"].value;
    var email = document.forms["myForm"]["email"].value;    
    var gender = document.forms["myForm"]["gender"].value;
    
    
    if (name==null || name=="") {
       alert("Name must be filled out");
        return false;
    }
    if (email==null || email=="") {
        alert("Email must be filled out");
        return false;
    }
    if (gender==null || gender=="") {
        alert("Body must be filled out");
        return false;
    }
    
    
}
</script>
